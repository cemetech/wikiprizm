---
revisions:
- author: Dr-carlos
  comment: Use GetVRAMAddress not 0xA8000000
  timestamp: '2022-03-17T10:43:57Z'
title: CopySpriteNbitMasked
---

## Synopsis

Copy an N-bit (for N=8, 2, or 1) sprite/image to the VRAM buffer,
treating one color as transparent.

### Definition

    #include <fxcg/display.h>
    void CopySpriteNbitMasked(const unsigned char* data, int x, int y, int width, int height, const color_t* palette, color_t maskColor, unsigned int bitwidth)  
     {
       color_t* VRAM = (color_t*) GetVRAMAddress();
       VRAM += (LCD_WIDTH_PX*y + x);
       int offset = 0;
       unsigned char buf;
       for(int j=y; j<y+height; j++)  
       {
          int availbits = 0;
          for(int i=x; i<x+width;  i++)  
          {
             if (!availbits)  
             {
                buf = data[offset++];
                availbits = 8;
             }
             color_t this = ((color_t)buf>>(8-bitwidth));
             color_t color = palette[this];
             if(color != maskColor)
             {
                *VRAM = color;
             }
             VRAM++;
             buf<<=bitwidth;
             availbits-=bitwidth;
          }
          VRAM += (LCD_WIDTH_PX-width);
       }
    }

### Inputs

const unsigned char\* data:

-   *int* **x**: X-coordinate of top-left of sprite
-   *int* **y**: Y-coordinate of top-left of sprite
-   *int* **width**: Width of sprite, in pixels
-   *int* **height**: Height of sprite, in pixels
-   *color_t\** **palette**: A palette, returned from SourceCoder
-   *color_t* **maskColor**: the color that should be treated as
    transparent
-   *unsigned int* **bitwidth**: The bit-width. This is designed for 8-,
    2-, and 1-bit images, but it would also work with 4-bit.

### Outputs

Sprite drawn to VRAM

## Comments

[SourceCoder](http://sc.cemetech.net) can be used to convert .png, .bmp,
and .gif images to arrays that you can use with this routine.

## Credits

m1ac4 KermMartian
