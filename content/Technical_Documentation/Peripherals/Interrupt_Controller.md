---
title: Interrupt Controller
aliases:
  - /Interrupt_Controller/
---

*This page has not been completed. Parts may be missing or reorganized
before completed. Information is provided as-is and may have errors.*

The SH7305 used in the Prizm contains one Interrupt Controller (INTC).
This component is tasked with prioritizing interrupt sources and
controlling interrupt requests to the CPU. The priority of each
interrupt can be set using the INTC registers. This interrupt controller
is compatible with the one found in the SH7724 and shares the same base
address, 0xA4080000.

The [OS]({{< ref "/OS_Information/" >}}) uses bit 1 of the Interrupt Mask
Register 9 (IMR9), at address 0xA40800A4, to mask the USB interrupt.

Bit 0 of IMR5 is used to mask interrupts for SCIF0 (serial).
