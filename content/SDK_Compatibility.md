---
revisions:
- author: Dr-carlos
  comment: Remove old miniSDK compatability information
  timestamp: '2021-10-07T20:25:15Z'
title: SDK_Compatibility
---

The original fx-9860-derived "miniSDK" contains some strange design
decisions. Some of these have been improved in libfxcg, mostly by moving
header files around and renaming some functions. The full list of
renamed functions is given in
[:Category:Renamed_Syscalls]({{< ref "Renamed_Syscalls/" >}}).

Many older programs will not compile successfully with the new libfxcg.
It used to be the case that defining the symbol `_FXCG_MINICOMPAT`
mapped old names to the new headers, but this has been removed.
