---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= c1 = == Description == This is the c1\nvalue\
    \ used in recursion. Can be used like the other variables. ==\nSyntax ==\u2019\
    \u2018\u2019c1\u2019\u2019\u2019 == Example == c1 ?\u2192c1 \\[\\[Category:BASIC_Comman\u2026\
    \u2019"
  timestamp: '2012-02-15T20:12:24Z'
title: C1
---

# c1

## Description

This is the c1 value used in recursion. Can be used like the other
variables.

## Syntax

**c1**

## Example

`c1`

`?→c1`
