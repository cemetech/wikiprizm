---
revisions:
- author: Turiqwalrus
  comment: grammar fix
  timestamp: '2012-02-29T20:20:36Z'
title: FuncOn
---

# FuncOn

## Description

This command sets the OS so it draws functions on the graphscreen.

## Syntax

**FuncOn**

## Example

`FuncOn`
