---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= BinomialCD( = == Description == This\ncommand\
    \ solves the binomial distribution with probability P, total\nnumber n, and event\u2019\
    \u2018x\u2019\u2018. This command follows the formula: nCx \\*\nP^x \u2026\u2019"
  timestamp: '2012-02-15T03:21:50Z'
title: BinomialPD(
---

# BinomialCD(

## Description

This command solves the binomial distribution with probability P, total
number n, and event *x*.

This command follows the formula: nCx \* P^x \* (1-P)^(n-x)

## Syntax

**BinomialPD(**\[*x*,\]n,P)

x is an optional value and can be either single value or list.

## Example

`BinomialPD(3,5,.6)`
