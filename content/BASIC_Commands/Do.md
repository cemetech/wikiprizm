---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Do = == Description == This command is\nused\
    \ to create Do-LoopWhile along with\u2019\u2018\u2019LpWhile\u2019\u2019\u2019\
    \ == Syntax ==\n\u2019\u2018\u2019Do\u2019\u2019\u2019 \u2026Codes\u2026 \u2019\
    \u2018\u2019LpWhile\u2019\u2019\u2019 \u2018\u2019Condition\u2019\u2019 == Example\
    \ == Do \u2026\u2019"
  timestamp: '2012-02-17T00:52:45Z'
title: Do
---

# Do

## Description

This command is used to create Do-LoopWhile along with **LpWhile**

## Syntax

**Do**

...Codes...

**LpWhile** *Condition*

## Example

    Do
    X+1→X
    LpWhile X<5
