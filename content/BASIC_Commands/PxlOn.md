---
title: PxlOn
---

## PxlOn

## Description

This command turns a pixel on the graph screen on.

## Syntax

`PxlOn <X-coordinate>,<Y-coordinate>`

## Example

`PxlOn 5,5`
