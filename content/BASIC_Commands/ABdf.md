---
revisions:
- author: YeongJIN COOL
  timestamp: '2012-02-15T23:31:11Z'
title: ABdf
---

# ABdf

## Description

This is the factor AB degrees of freedom. It is used like a number.
ANOVA must be run in order to use this.

## Syntax

**ABdf**

## Example

`ABdf`
