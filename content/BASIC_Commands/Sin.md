---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= sin = == Description == This command\nreturns\
    \ the sine of the value. == Syntax ==\u2019\u2018\u2019sin\u2019\u2019\u2019 \u2018\
    \u2019value\u2019\u2019 ==\nExample == sin 30 \\[\\[Category:BASIC_Commands\\\
    ]\\]\u2019"
  timestamp: '2012-02-15T03:46:45Z'
title: Sin
---

# sin

## Description

This command returns the sine of the value.

## Syntax

**sin** *value*

## Example

`sin 30`
