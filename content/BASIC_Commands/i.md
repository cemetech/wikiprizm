---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018=\u2019\u2018i\u2019\u2019 = == Description ==\
    \ \u2018\u2019i\u2019\u2019 is the\nimaginary number. you can perform complex\
    \ calculations using this.\nThis can be used as \u201Cnumber\u201D. == Syntax\
    \ == \u2019\u2019\u2019\u2018\u2019i\u2019\u2019\u2019\u2019\u2019 == Example\n\
    ==\u2026\u2019"
  timestamp: '2012-02-15T20:20:43Z'
title: i
---

# *i*

## Description

*i* is the imaginary number. you can perform complex calculations using
this. This can be used as "number".

## Syntax

***i***

## Example

*`i`*

    5+3

*`i`*
