---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= b(Reg) = == Description == This is\nthe\u2019\
    \u2018\u2019b\u2019\u2019\u2019 value used in regression. Can be treated as a\
    \ number.\nRegression including \u2019\u2018\u2019b\u2019\u2019\u2019 must be\
    \ calculated before usage. ==\nSyntax ==\u2026\u2019"
  timestamp: '2012-02-15T20:03:21Z'
title: B(Reg)
---

# b(Reg)

## Description

This is the **b** value used in regression. Can be treated as a number.
Regression including **b** must be calculated before usage.

## Syntax

**b**

## Example

`b`

`b+4`
