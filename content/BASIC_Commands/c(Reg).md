---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= c(Reg) = == Description == This is\nthe\u2019\
    \u2018\u2019c\u2019\u2019\u2019 value used in regression. Can be treated as a\
    \ number.\nRegression including \u2019\u2018\u2019c\u2019\u2019\u2019 must be\
    \ calculated before usage. ==\nSyntax ==\u2026\u2019"
  timestamp: '2012-02-15T20:05:28Z'
title: c(Reg)
---

# c(Reg)

## Description

This is the **c** value used in regression. Can be treated as a number.
Regression including **c** must be calculated before usage.

## Syntax

**c**

## Example

`c`

`c+4`
