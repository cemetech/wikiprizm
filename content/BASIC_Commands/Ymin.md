---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= YMin = == Description == This command\nsets\
    \ the minimum y value displayed in the graphscreen. == Syntax\n==\u2019\u2018\
    Value\u2019\u2018\u2192YMin == Example == 3\u2192\u2019\u2018\u2019YMin\u2019\u2019\
    \u2019\n\\[\\[Category:BASIC_Com\u2026\u2019"
  timestamp: '2012-02-24T20:31:41Z'
title: Ymin
---

# YMin

## Description

This command sets the minimum y value displayed in the graphscreen.

## Syntax

*Value*→YMin

## Example

`3→`**`YMin`**
