---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= a+bi = == Description == This command\nsets\
    \ calculator to display complex value if needed. == Syntax\n==\u2019\u2018\u2019\
    a+bi\u2019\u2019\u2019 == Example == a+bi \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-15T12:08:55Z'
title: a+bi
---

# a+bi

## Description

This command sets calculator to display complex value if needed.

## Syntax

**a+bi**

## Example

`a+bi`
