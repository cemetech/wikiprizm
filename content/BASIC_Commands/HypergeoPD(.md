---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= HypergeoPD( = == Description == This\ncommand\
    \ returns the hypergeometric distribution at event x. The\nformula of this command\
    \ is: P(x)=kCx \\* (N-k)C(n-x) / NCn == Syntax\n==\u2019\u2018\u2019H\u2026\u2019"
  timestamp: '2012-02-16T00:35:52Z'
title: HypergeoPD(
---

# HypergeoPD(

## Description

This command returns the hypergeometric distribution at event x.

The formula of this command is: P(x)=kCx \* (N-k)C(n-x) / NCn

## Syntax

**HypergeoPD(***x*,*n*,*k*,*N***)**

## Example

`HypergeoPD(3,10,5,100)`
