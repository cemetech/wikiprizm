---
revisions:
- author: YeongJIN COOL
  timestamp: '2012-02-29T20:00:34Z'
title: SortA(
---

## Description

This command sorts a list's elements in ascending order(low to high).

## Syntax

SortA(\<List name>)

## Example

`{1,45,2,3,5,2} → List 1              //Data is stored into List 1, then the list is sorted in ascending  order and displayed at (1,1).`

`SortA(List 1)                       //This example would display {1,2,2,3,5,45} as the end result.`

`Locate 1,1,List 1`
