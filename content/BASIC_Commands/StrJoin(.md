---
title: StrJoin(
---

==StrJoin(

## Description

Connects two strings together

## Syntax

`StrJoin(<First string>,<Second string>)`

## Example

    "EGGPLANT " → Str 1
    "SOUFFLE" → Str 2
    StrJoin(Str 1,Str 2)            //Displays EGGPLANT SOUFFLE
