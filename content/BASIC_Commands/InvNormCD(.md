---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= InvNormCD( = == Description == This\ncommand\
    \ returns the cumulative inverse normal distribution. ==\nSyntax ==\u2019\u2018\
    \u2019InvNormCD(\u2019\u2019\u2018\\[\u2019\u2019L(or -1) or R(or 1) or C(or\n\
    0),\u2019\u2019\\]\u2019\u2018p\u2019\u2019 \\[\u2018\u2019s\u2026\u2019"
  timestamp: '2012-02-16T00:56:10Z'
title: InvNormCD(
---

# InvNormCD(

## Description

This command returns the cumulative inverse normal distribution.

## Syntax

**InvNormCD(**\[*L(or -1) or R(or 1) or C(or 0),*\] *p*
\[*std_dev*,*mean*\]**)**

## Example

`InvNormCD(.3,3,37)`
