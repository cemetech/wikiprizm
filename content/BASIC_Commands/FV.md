---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= FV = == Description == This is the\nfinal value\
    \ in financial calculation. Can be used as a variable. ==\nSyntax ==\u2019\u2018\
    \u2019FV\u2019\u2019\u2019 == Example == FV 100\u2192FV\n\\[\\[Category:BASIC_Command\u2026\
    \u2019"
  timestamp: '2012-02-16T23:31:54Z'
title: FV
---

# FV

## Description

This is the final value in financial calculation. Can be used as a
variable.

## Syntax

**FV**

## Example

    FV
    100→FV
