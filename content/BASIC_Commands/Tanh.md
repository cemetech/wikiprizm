---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= tanh = == Description == This command\nreturns\
    \ the hyperbolic tangent of the value. == Syntax ==\u2019\u2018\u2019tanh\u2019\
    \u2019\u2019\n\u2018\u2019Value\u2019\u2019 == Example == tanh 2\u2019"
  timestamp: '2012-02-29T20:24:43Z'
title: Tanh
---

# tanh

## Description

This command returns the hyperbolic tangent of the value.

## Syntax

**tanh** *Value*

## Example

`tanh 2`
