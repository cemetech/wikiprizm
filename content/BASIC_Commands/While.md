---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= While = == Description == This command\nis used\
    \ to create While loop along with\u2019\u2018\u2019WhileEnd\u2019\u2019\u2018\
    . == Syntax\n==\u2019\u2018\u2019While\u2019\u2019\u2019 \u2018\u2019Condition\u2019\
    \u2019 Code\u2026 \u2019\u2018\u2019WhileEnd\u2019\u2019\u2019 == Example == W\u2026\
    \u2019"
  timestamp: '2012-02-24T20:26:35Z'
title: While
---

# While

## Description

This command is used to create While loop along with **WhileEnd**.

## Syntax

**While** *Condition*

Code...

**WhileEnd**

## Example

    While X=3
    X+1→X
    WhileEnd
