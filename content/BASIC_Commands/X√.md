---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= x\u221A = == Description == This command\nreturns\
    \ the xth root of the value. == Syntax\n==\u2019\u2018Value\u2019\u2019\u2019\u2018\
    \u2019x\u221A\u2019\u2019\u2019\u2018\u2019Value\u2019\u2019 == Example == 4x\u221A\
    150\n\\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-24T20:15:22Z'
title: "X\u221A"
---

# x√

## Description

This command returns the xth root of the value.

## Syntax

*Value***x√***Value*

## Example

`4x√150`
