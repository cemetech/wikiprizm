---
title: SortD(
---

## Description

This command sorts a list's elements in descending order(high to low).

## Syntax

SortD(\<List name>)

## Example

`{1,45,2,3,5,2} → List 1              //Data is stored into List 1, then the list is sorted in descending  order and displayed at (1,1).`

`SortD(List 1)                       //This example would display {45,5,3,2,2,1} as the end result.`

`Locate 1,1,List 1`
