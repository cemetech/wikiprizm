---
revisions:
- author: Turiqwalrus
  comment: /\* Example \*/
  timestamp: '2012-02-29T18:29:25Z'
title: RanInt
---

## Description

Outputs a random integer given lower and upper bounds. This function can
also output a list of random numbers if an optional third argument is
added

## Syntax

`RanInt#(<Lower bound>,<Upper bound>,<number of elements(optional)>)`

## Example

RanInt#(1,15) → A //stores a random number from 1 to 15 in the variable
A

RanInt#(1,15,5) → List 1 //stores a list containing **five** random
numbers from 1 to 15 into 'List 1'
