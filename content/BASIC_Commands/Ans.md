---
revisions:
- author: KermMartian
  timestamp: '2011-12-04T15:48:24Z'
title: Ans
---

## Description

Ans recalls the last value that was processed from either the
'Run-Matrix' add-in, or from inside a program. It does not store List or
String information.

## Syntax

Ans

## Example

    25->A:25->B
    A+B
    Locate 1,1,Ans
