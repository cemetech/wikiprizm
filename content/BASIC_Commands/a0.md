---
revisions:
- author: YeongJIN COOL
  comment: /\* Example \*/
  timestamp: '2012-02-15T20:01:27Z'
title: a0
---

# a0

## Description

This is the a0 value used in recursion. Can be used like the other
variables.

## Syntax

**a0**

## Example

`a0`

`?→a0`
