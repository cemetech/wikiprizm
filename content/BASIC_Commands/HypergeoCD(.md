---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= HypergeoCD( = == Description == This\ncommand\
    \ returns the cumulative hypergeometric distribution until\nevent x. == Syntax\
    \ ==\u2019\u2018\u2019HypergeoCD(\u2019\u2019\u2019\u2018\u2019x\u2019\u2018,\u2019\
    \u2018n\u2019\u2018,\u2019\u2018k\u2019\u2018,\u2019\u2018N\u2019\u2019\u2019\u2019\
    \u2018)\u2019\u2019\u2019\n==\u2026\u2019"
  timestamp: '2012-02-16T00:36:31Z'
title: HypergeoCD(
---

# HypergeoCD(

## Description

This command returns the cumulative hypergeometric distribution until
event x.

## Syntax

**HypergeoCD(***x*,*n*,*k*,*N***)**

## Example

`HypergeoCD(3,10,5,100)`
