---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= AxesOn = == Description == This command\nsets\
    \ graph screen to display axes. == Syntax ==\u2019\u2018\u2019AxesOn\u2019\u2019\
    \u2019 ==\nExample == AxesOn \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-15T12:10:17Z'
title: AxesOn
---

# AxesOn

## Description

This command sets graph screen to display axes.

## Syntax

**AxesOn**

## Example

`AxesOn`
