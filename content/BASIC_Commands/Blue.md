---
revisions:
- author: YeongJIN COOL
  timestamp: '2012-02-15T12:05:07Z'
title: Blue
---

# Blue

## Description

This command sets the subsequent Locate or other text command to render
in blue. This command can be found at \[Shift\] \[5\] \[1\].

## Syntax

**Blue \<text command>**

## Example

`Blue Locate 1, 1, "Asdf`
