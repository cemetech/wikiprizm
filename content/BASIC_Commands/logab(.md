---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= logab( = == Description == This command\nreturns\
    \ the value of log b base a. == Syntax\n==\u2019\u2018\u2019logab(\u2019\u2019\
    \u2019\u2018\u2019base\u2019\u2018,\u2019\u2018value\u2019\u2019\u2019\u2019\u2018\
    )\u2019\u2019\u2019 == Example == logab(2,8)\n\\[\\[Category:BASIC_Comma\u2026\
    \u2019"
  timestamp: '2012-02-16T23:18:21Z'
title: logab(
---

# logab(

## Description

This command returns the value of log b base a.

## Syntax

**logab(***base*,*value***)**

## Example

`logab(2,8)`
