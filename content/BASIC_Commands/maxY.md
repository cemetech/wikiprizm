---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= maxY = == Description == This is for\nspecifying\
    \ Maximum y value in y axis at graphscreen. == Syntax\n==\u2019\u2018Value\u2019\
    \u2018\u2192\u2019\u2018\u2019MaxY\u2019\u2019\u2019 == Example == 3\u2192MaxY\u2019"
  timestamp: '2012-02-17T00:59:55Z'
title: maxY
---

# maxY

## Description

This is for specifying Maximum y value in y axis at graphscreen.

## Syntax

*Value*→**MaxY**

## Example

`3→MaxY`
