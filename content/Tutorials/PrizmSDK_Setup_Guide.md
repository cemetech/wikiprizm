---
revisions:
- author: Tari
  comment: Remove references to ancient versions of GCC and binutils
  timestamp: '2022-10-11T04:55:42Z'
title: PrizmSDK_Setup_Guide
---

The PrizmSDK is a community-created SDK for writing C/C++ programs for
the Casio Prizm. Casio, as of now, has shown no plans of making and
releasing an official SDK. The PrizmSDK contains headers, libraries,
scripts, and a build system for developing programs using GCC.

**If you want to develop add-ins on Windows, refer to the [libfxcg
documentation](https://github.com/Jonimoose/libfxcg/blob/master/docs/howto-windows.md)
for setup.** Otherwise, read on to set up a toolchain.

If you're migrating from the "Mini SDK" or attempting to compile old
programs, it is no longer possible to do this without changing function
names manually.

## Getting libraries {#getting_libraries}

Download the compiled libraries and include files from [libfxcg's GitHub
releases](https://github.com/Jonimoose/libfxcg/releases/latest), or, if
you are running linux, download the github repository and

`$ make`

To generate the compiled libraries.

## Setting up a toolchain on Linux {#setting_up_a_toolchain_on_linux}

The SDK currently only comes with pre-built Windows binaries. In order
to compile .g3a's using a native compiler, GCC must be built from
source.

### Arch Linux {#arch_linux}

For Arch users, there are packages in the AUR that provide the necessary
toolchain:

-   [sh3eb-elf-binutils](https://aur.archlinux.org/packages/sh3eb-elf-binutils/)
-   [mkg3a](https://aur.archlinux.org/packages/mkg3a/)

sh3eb-elf-gcc is also required, but this AUR package is broken and out
of date. A replacement PKGBUILD can be cloned [here
instead](https://gitlab.com/dr_carlos/sh3eb-elf-gcc), or you can just
patch the sh3eb-elf-gcc AUR PKGBUILD yourself.

Install them as you would any other AUR package. From there, you will
need to [configure](#Configuring_the_PrizmSDK) the SDK package to use
the PREFIX /usr/bin/sh3eb-elf-.

### Other Distributions {#other_distributions}

For other distributions that do not provide packages for SuperH GCC and
binutils, you will need to compile the toolchain yourself. If you have
gcc compilation problems, check [this
topic](http://www.cemetech.net/forum/viewtopic.php?p=216509).

#### Getting the Needed Packages {#getting_the_needed_packages}

In order to build a compiler, you must grab a [GCC
package](https://ftp.gnu.org/gnu/gcc/) (non-core) and a [binutils
package](https://ftp.gnu.org/gnu/binutils/). You should generally use
the most recent version of each that is available.

In addition to these core source packages, you must ensure that you have
the required development headers installed on your system. Make sure
that you have the development packages for:

-   mpfr
-   mpc
-   gmp
-   libpng

Also, make sure that you have the following packages installed as well:

-   ppl-pwl

#### Setting up the Build Environment {#setting_up_the_build_environment}

For building the needed packages, /usr/src/ will be used for building
and /usr/local/cross/ will be used for output. For this guide, please
preserve your shell session.

Firstly, open up your preferred terminal emulator. Navigate to
/usr/src/, creating it if not already made, and change the mode of src/
for global read/write access. Also while root, create /usr/local/cross.

    $ cd /usr
    $ su
    # chmod a+rw src
    # mkdir /usr/local/cross
    # exit
    $ cd src
    $ touch a

If you were able to run *touch a* without problems, then you have access
to the src folder. Next, you will set up the build structure in the src/
folder.

`$ mkdir build-gcc build-binutils`

You should have the two folders created. Extract the two downloaded
packages into /usr/src/, named gcc and binutils. You should have two
folders, /usr/src/gcc and /usr/src/binutils with the extracted files in
them.

The build-\* folders are used for all of the temporary files generated
when building these packages. If you build in gcc/ and make a mistake
and mess up the build system, you must remove it and extract again. If
you make a mistake in build-gcc, then you just have to delete and remake
the build-gcc folder because all of the Makefiles and temp files are
only placed in the build folders.

#### Compiling binutils {#compiling_binutils}

Continuing from your already existing terminal session, go into the
build-binutils folder and begin configuring your build with

    $ cd build-binutils
    $ ../binutils/./configure --target=sh3eb-elf --prefix=/usr/local/cross --disable-nls

If you have any missing headers errors, search and install the missing
files. The exact way to do so depends on a per-distro basis.

Once the configure succeeds, you may proceed to build binutils and
install using

    $ make
    $ make install

**Helpful hint:** If you have a multicore CPU with RAM to spare, you can
speed up builds by using \`*make -j\$(nproc)*\` instead of \`*make*\`
for this guide.

#### Compiling GCC {#compiling_gcc}

Now that binutils have been properly set up and installed, you can
proceed to building GCC. Verify that you downloaded the gcc-x.x.x
package and not the gcc-core-x.x.x (or others). Do not forget to do this
step or else the configuring will fail:

`export PATH=$PATH:/usr/local/cross/bin`

Once the above is done, proceed to configure GCC by continuing from
above:

    $ cd /usr/src/build-gcc
    $ ../gcc/./configure --target=sh3eb-elf --prefix=/usr/local/cross --disable-nls --enable-languages=c,c++ --without-headers

Most likely, you will have errors for missing headers. Install all of
the and reconfigure if needed. Once configuring completes, start
building the specific components of GCC using:

    $ make all-gcc
    # make install-gcc

You now have GCC installed, but libgcc hasn't been built. You can build
and install it using:

    $ make all-target-libgcc
    # make install-target-libgcc

#### Another way to compile {#another_way_to_compile}

The above method will get the job done. However, it is easier to just
run this command. Note: before running this, please create a folder
where you want the GNU toolchain to be installed, and replace
InsertPathHere with the full path of the directory. For instance, you
could replace InsertPathHere with \$HOME/casio-gcc. The above guide ran
as root, but that is not necessary if you are going to be installing
this in a folder that the current user has sufficient permissions to
access.

`tar -xf binu* && tar -xf gcc* && export PREFIX=InsertPathHere && export PATH=$PATH:$PREFIX/bin && export TARGET=sh3eb-elf && export CFLAGS="-O2 -pipe -s -fomit-frame-pointer -ffunction-sections -fdata-sections" && export CXXFLAGS=$CFLAGS && export LDFLAGS="-Wl,--gc-sections" && mkdir build-binutils && cd build-binutils && ../binutils-2.*/configure --disable-werror --target=$TARGET --prefix=$PREFIX --disable-nls --disable-tls --disable-libssp && make && make install && cd .. && mkdir build-gcc && cd build-gcc && ../gcc-4.*/configure --target=$TARGET --prefix=$PREFIX --enable-sjlj-exceptions --disable-hosted-libstdcxx --with-gnu-as --with-gnu-ld --enable-languages=c,c++ --disable-nls --disable-tls --disable-libssp --disable-threads --disable-shared --disable-__cxa_atexit && make all-gcc && make install-gcc && make all-target-libgcc && make install-target-libgcc`

#### Compiling mkg3a {#compiling_mkg3a}

[mkg3a](https://gitlab.com/taricorp/mkg3a) (not to be confused with the
PrizmSDK itself) is a tool used in the PrizmSDK to take the binary ELF
output and the icons and package them into a .g3a that can be sent to
the Prizm calculator. The SDK comes with a Windows version, but a Linux
build can be done. In order to build a stable release, [download
mkg3a](https://gitlab.com/taricorp/mkg3a/-/archive/master/mkg3a-master.tar.gz)
and extract it; location does not matter. Make sure that you have
*cmake* installed on your system before proceeding.

In the terminal session, navigate to the mkg3a directory and run

`$ cmake .`

This will create the needed Makefiles. You can proceed to build and
install using:

    $ make
    $ su -c 'make install'

Enter your root password when prompted to when installing.

#### FAQ

I get this error when trying to build gcc/libgcc: (Linux)

    checking for suffix of object files... 
    configure: error: in `/usr/src/build-gcc/sh3eb-elf/libgcc': "configure: error: cannot compute suffix of object files: cannot compile "

This is caused by the configure script not finding the binaries made by
binutils.

1.  Make sure that you ran *export PATH=\$PATH:/usr/local/cross/bin*
    before doing ./configure \[rest of the parameters\]
2.  If you do have the path adjusted, verify that you do have binaries
    like sh3eb-elf-as, sh3eb-elf-ld, etc in /usr/local/cross/bin. If
    not, do *make install* again in your build-binutils folder.
3.  If you know that the binaries are there, you can run them when they
    are in your \$PATH, but it still fails, make a thread on [Cemetech's
    Prizm subforum](http://www.cemetech.net/forum/viewforum.php?f=68).

### Configuring the PrizmSDK {#configuring_the_prizmsdk}

Once you've downloaded or cloned the [Prizm
SDK](https://github.com/Jonimoose/libfxcg), edit the
toolchain/prizm_rules file, modifying the *PREFIX...* line to read ONE
of the following, depending on where your cross-compiler toolchain is:

    PREFIX := /usr/local/cross/bin/sh3eb-elf-
    PREFIX := /usr/bin/sh3eb-elf-

Once everything above has been completed, you can use examples/skeleton
as a starting point for making your projects. To test out the SDK, run

`$ make`

This should build the src/main.c file and create a .g3a addin.
