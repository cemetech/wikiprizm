---
title: Bitmaps
aliases:
  - /Bitmaps/
---

*This page has not been completed. Parts may be missing or reorganized
before completed. Information is provided as-is and may have errors.*

The OS contains a variety of bitmaps, with different sizes and color
depths. Most of these bitmaps are shown to the user in the easily
accessible OS screens, meant for common usage; others are never shown
except on test and diagnostic screens.

## Visible bitmaps {#visible_bitmaps}

This is a certainly incomplete list of the bitmaps that are shown to the
user during common calculator usage.

Please list their dimensions, format and address in memory when known
(specify the OS version, including minor digits/revision).

-   Main Menu app icons (16 bpp, RGB565) - this includes all the icons
    for the built-in OS apps, from Run-Matrix to System.
-   Main Menu graphics (16 bpp, RGB565) - this includes the top bar, the
    graphics for the scroll indicator to the right, the 3D-look battery
    icon and keyboard status indicators, and the labels shown on the top
    right corner of each app to indicate its shortcut (each has a
    "selected" and "unselected" version).
-   [Function key labels]({{< ref "FKey_Bitmaps.md" >}}) (1 bpp) - the
    labels shown on the bottom of the screen throughout the OS, to
    indicate the action of each function key.
-   [Screen
    backgrounds]({{< ref "Syscalls/UI_elements/SetBackGround.md" >}})
    (384x192 px, 16 bpp, RGB 565) - the backgrounds shown on some
    screens, like the eActivity file browser, the matrix list, the
    program list, file transfer screens or the System menu.
-   Status bar indicators (various dimensions, some appear to be 1 bpp,
    others are definitely 3 bpp) - the labels shown on the status bar,
    including the battery indicator, the keyboard status indicator, the
    indicator shown on the top right when seeing a chart with color link
    on, the eActivity memo indicator, etc.
-   Busy-indicator bitmaps (3 bpp) - shown on the top right when the
    system is busy.
-   All the [font]({{< ref "Fonts.md" >}}) characters as bitmaps
    (various dimensions, 1 bpp)
-   Scrollbar, progressbar and message box assets (3 bpp) - some of
    these may not be bitmaps (they may be drawn manually each time, for
    higher flexibility), but it's very likely that some like the rounded
    corners on the tabs of the conversion menus are bitmaps.
-   File type and folder icons (18x24 px, 16 bpp, RGB 565) - shown on
    file browsers
-   Casio logo shown on power off (16 bpp, RGB 565) - at least some OS
    versions have two copies of this one, one near the start of the
    flash and another before the screen backgrounds.
-   eActivity strip icons for built-in apps (3 bpp)
-   eActivity horizontal rule (3 bpp) - open eActivity document, press
    F6, F3, F3. It's possible that this is drawn as a composition of
    lines and area fills each time.
-   eActivity memo icons and popups (3 bpp) - it's likely that these are
    drawn manually every time, and are not bitmaps.
-   ...

## Usually not visible bitmaps {#usually_not_visible_bitmaps}

Apart from function key labels which are not used or are only used on
the [TestMode]({{< ref "Syscalls/TestMode.md" >}}), the OS contains at
least six bitmaps intended for testing the screen and which can be
displayed from the [diagnostic
mode]({{< ref "Secret_Key_Combinations.md" >}}) by pressing 3, 1, 1 (to
set the color mode to 16 bpp), 3, 6, then F1 to F6 to choose picture
1-6.

Here are the pictures in question:

[1](http://s.lowendshare.com/10/1416327695.607.diagpic1.png)
[2](http://s.lowendshare.com/10/1416327734.54.diagpic2.png)
[3](http://s.lowendshare.com/10/1416327754.832.diagpic3.png)
[4](http://s.lowendshare.com/10/1416327767.64.diagpic4.png)
[5](http://s.lowendshare.com/10/1416327776.726.diagpic5.png)
[6](http://s.lowendshare.com/10/1416327787.748.diagpic6.png)

The first two clearly seem to be taken out of the Windows example images
folder. The third one appears to be from the island of Guam. Google
Image search can't find any page where the fourth is used.

The fifth image appears to be a screenshot of a prototype version of the
graph drawing screen. Below is a screenshot of the final graph screen,
drawing the same equations, with the settings made to match as closely
as possible what's seen on the diagnostic picture. On the original
picture, note the placeholder for the statusbar, the shorter axis
tick-marks and the rendering defect on the first lines of the graph
(which has been solved in the final version).

[7](http://s.lowendshare.com/10/1416328968.195.diagemulation.png)

The first five test images are saved with no compression, at two bytes
per pixel, meaning 829440 bytes of flash space are used with these
example pictures. The sixth image is probably generated at run-time.
