---
title: USB Communication
aliases:
  - /USB_Communication/
---

*This page has not been completed. Parts may be missing or reorganized
before completed. Information is provided as-is and may have errors.*

*If you have any documentation or have played around with
the USB communication for the Prizm, please contribute your findings
by editing this page. Thank you!*

The Prizm includes a mini-B female USB connector. Over a USB 2.0
connection, users can manage the files on the calculator, mirror the
calculator screen to a computer or certain Casio projectors, and update
the [Operating System]({{< ref "/OS_Information/" >}}). Of these operations,
only file management can be done without the use of additional software
or drivers, as in this mode, the calculator registers as a Mass-Storage
Device. This allows for transferring files even in operating systems
other than Microsoft Windows.

The screen mirroring functionality requires that Screen Receiver, which
is freeware software from Casio available for Windows and Mac OS X, be
installed on the computer. The OS update functionality requires a OS
updater to be running on the computer.

The calculator can be used normally, without entering any of the USB
modes, while a powered USB cable is connected. This is done by pressing
EXIT on the USB mode selection screen. When powered over USB, the
calculator takes no or very little battery power. It doesn't support
battery charging.

Pressing EXIT on the USB mode selection screen and going into the System
app is also necessary to perform a OS update, as instructed by the
updater.

## Mass Storage mode {#mass_storage_mode}

In this mode, which can be selected by pressing F1 on the dialog that
appears when connecting the USB cable, the calculator emulates a USB
Flash Drive. Before initiating the connection, the OS serializes all the
data in the [Main Memory]({{< ref "File_System.md" >}}) to the \@MainMem
folder in [Storage Memory]({{< ref "File_System.md" >}}), which is not
visible from the file lists displayed by the OS on the calculator. After
terminating the USB connection, the calculator translates the contents
of the \@MainMem folder back into the Main Memory, replacing the
existing ones (so that if the \@MainMem folder is deleted, all data in
the Main Memory is deleted) and placing unrecognized files in a folder
named "SAVE-F". In fact, this process is detailed starting on page 13-5
of the Prizm's Software Manual, including the file names associated with
each Main Memory item.

The computer should never be used to format the Storage Memory. Doing so
will prevent the calculator from being used, as it will keep displaying
"File System ERROR" - presumably due to not being able to mount the
specially-formatted FAT file system in [Flash]({{< ref "Flash.md" >}}).
The only way to get out of this situation is to perform a general reset
of the calculator, making the OS format the file system in its own
terms.

## Screen Receiver mode {#screen_receiver_mode}

The calculator has two Screen Receiver modes: one for connection to a
Windows XP computer and another for connection to other (more recent)
operating systems. As explained above, both need the Screen Receiver
software to be installed on the computer. Certain Storage Memory
operations are not available in this mode.

Please see [Screen_Receiver]({{< ref "Screen_Receiver.md" >}}) for more
details.

## Projector mode {#projector_mode}

Not much is known about this mode, mainly because it requires the use of
specific hardware - the Casio projectors. It is known that, in earlier
[OS]({{< ref "/OS_Information/" >}}) versions at least, this mode had certain
problems that caused instability on the calculator, including [System
ERRORs]({{< ref "Error_handling.md" >}}). The Prizm's software manual
even includes some suggestive paragraphs on troubleshooting this mode,
including instructions for reconnecting the USB cable and power-cycling
the projector. This gives the impression that Casio engineers did not
have much access to testing hardware, or that the decision to support
the Projector mode was taken very late in the development process, not
giving enough time for proper testing and error-proofing of the code.

## OS Update mode {#os_update_mode}

This is a special mode that waits for a OS Updater to issue special
commands over USB, and implements [Protocol
7]({{< ref "USB_Communication.md" >}}) at least partially.

## Protocol

The Prizm uses the MSD USB protocol for Drive mode, Screen Receive, and
OS Updates (both modes). For the extra data, it uses UFI bulk-mode data
methods.

## MSD CBW Description {#msd_cbw_description}

|       |                        |              |     |     |     |     |     |     |
|-------|------------------------|--------------|-----|-----|-----|-----|-----|-----|
| Bit   | 7                      | 6            | 5   | 4   | 3   | 2   | 1   | 0   |
| 0-3   | bCBWSignature          |              |     |     |     |     |     |     |
| 4-7   | dCBWTag                |              |     |     |     |     |     |     |
| 8-11  | dCBWDataTransferLength |              |     |     |     |     |     |     |
| 12    | bmCBWFlags             |              |     |     |     |     |     |     |
| 13    | Reserved (0)           | bCBWLUN      |     |     |     |     |     |     |
| 14    | Reserved (0)           | bCBWCBLength |     |     |     |     |     |     |
| 15-30 | CBWCB                  |              |     |     |     |     |     |     |

-   *dCBWSignature* - Always 0x43425355 in little endian. Indicates a
    CBW.
-   *dCBWTag* - A 4 byte tag sent by the host, echoed back by the
    peripheral in a CSW.
-   *dCBWDataTransferLength* - Amount of bytes, in little endian,
    expected to be sent/received on a bulk pipe directly after sending
    the packet.
-   *bmCBWFlags* - 0x00 if sending host to device, 0x80 if device to
    host. Ignored if dCBWDataTransferLength is 0.
-   *bCBWLUN* - Set to 0.
-   *bCBWCBLength* - Length of usable data in CBWCB. Doesn't mean packet
    size is different!
-   *CBWCB* - Command block to be executed. For the Prizm, this is a UFI
    command block.

## MSD CSW Description {#msd_csw_description}

|      |                 |     |     |     |     |     |     |     |
|------|-----------------|-----|-----|-----|-----|-----|-----|-----|
| Bit  | 7               | 6   | 5   | 4   | 3   | 2   | 1   | 0   |
| 0-3  | dCSWSignature   |     |     |     |     |     |     |     |
| 4-7  | dCSWTag         |     |     |     |     |     |     |     |
| 8-11 | dCSWDataResidue |     |     |     |     |     |     |     |
| 12   | bCSWStatus      |     |     |     |     |     |     |     |

-   *dCSWSignature* - Always 0x53425355 in little endian. Indicates a
    CSW.
-   *dCSWTag* - Set to the dCBWTag of the received CBW.
-   *dCSWDataResidue* - When receiving data from the host, this is the
    difference between the amount of data expected in
    *dCBWDataTransferLength* and the actual amount of data processed.
    For sending, it is the difference between *dCBWDataTransferLength*
    and actual bytes sent.
-   *bCSWStatus* - Indicates the success or failure. Responses are below

|               |                     |
|---------------|---------------------|
| Value         | Description         |
| 0x00          | Command Passed      |
| 0x01          | Command Failed      |
| 0x02          | Phase Error         |
| 0x03 and 0x04 | Reserved (Obsolete) |
| 0x05 to 0xFF  | Reserved            |

If there is an error, you should perform a phase recovery.

## MSD UFI Command Block Description {#msd_ufi_command_block_description}

|         |                                                                             |          |     |     |     |     |     |     |
|---------|-----------------------------------------------------------------------------|----------|-----|-----|-----|-----|-----|-----|
| Bit     | 7                                                                           | 6        | 5   | 4   | 3   | 2   | 1   | 0   |
| 0       | Operation Code                                                              |          |     |     |     |     |     |     |
| 1       | Logic Unit Number                                                           | Reserved |     |     |     |     |     |     |
| 2 (MSB) | Logical Block Address                                                       |          |     |     |     |     |     |     |
| 3       |                                                                             |          |     |     |     |     |     |     |
| 4       |                                                                             |          |     |     |     |     |     |     |
| 5 (LSB) |                                                                             |          |     |     |     |     |     |     |
| 6       | Reserved                                                                    |          |     |     |     |     |     |     |
| 7 (MSB) | Transfer or Parameter list or Allocation Length (usually allocation length) |          |     |     |     |     |     |     |
| 8 (LSB) |                                                                             |          |     |     |     |     |     |     |
| 9       | Reserved                                                                    |          |     |     |     |     |     |     |
| 10      | Reserved                                                                    |          |     |     |     |     |     |     |
| 11      | Reserved                                                                    |          |     |     |     |     |     |     |

## MSD UFI Extended Command Block Description {#msd_ufi_extended_command_block_description}

|         |                                                                             |          |     |     |     |     |     |     |
|---------|-----------------------------------------------------------------------------|----------|-----|-----|-----|-----|-----|-----|
| Bit     | 7                                                                           | 6        | 5   | 4   | 3   | 2   | 1   | 0   |
| 0       | Operation Code                                                              |          |     |     |     |     |     |     |
| 1       | Logic Unit Number                                                           | Reserved |     |     |     |     |     |     |
| 2 (MSB) | Logical Block Address                                                       |          |     |     |     |     |     |     |
| 3       |                                                                             |          |     |     |     |     |     |     |
| 4       |                                                                             |          |     |     |     |     |     |     |
| 5 (LSB) |                                                                             |          |     |     |     |     |     |     |
| 6 (MSB) | Transfer or Parameter list or Allocation Length (usually allocation length) |          |     |     |     |     |     |     |
| 7       |                                                                             |          |     |     |     |     |     |     |
| 8       |                                                                             |          |     |     |     |     |     |     |
| 9 (LSB) |                                                                             |          |     |     |     |     |     |     |
| 10      | Reserved                                                                    |          |     |     |     |     |     |     |
| 11      | Reserved                                                                    |          |     |     |     |     |     |     |

## Prizm UFI Commands {#prizm_ufi_commands}

|      |      |            |                                          |                                                                                                                                                              |
|------|------|------------|------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ID   | Name | Parameters | Returns                                  | Comments                                                                                                                                                     |
| 0xC0 | Poll | todo       | The prizm returns a 0xD0 response. Todo. | Basically used to see if the Prizm is done processing the last sent data and is ready for more, and also checks to see if the Prizm wants to send something. |

### 0xC0

Since the peripheral cannot initiate a data send on its own, the Prizm
gets polled constantly to check for any activity. Here is the packet
layout:

**C0**

Custom return data:

**D0** 00 00 00 00 00 00 *SS SS* 00 *AA AA* 00 00 00 00

-   *SS SS* - Amount of bytes as big endian that the Prizm wants to send
-   *AA AA* - Activity status of the Prizm. If value is not **10 00**,
    then the Prizm is busy and cannot accept more data.

### 0xC1

Receives data. Should be sent if 0xC0 command revealed that the Prizm
wants to send stuff. Syntax:

**C1** 00 00 00 00 00 *SS SS*

-   *SS SS* - Amount of bytes to be sent.

Once this is sent, the Prizm sends raw data.

### 0xC2

This command is used to send raw data to the Prizm. It seems the first
byte of the raw data is not part of the data. The raw data usually is
made up of character-encoded hex at the start. Command syntax:

**C2** 00 00 00 00 00 *SS SS*

-   *SS SS* - Amount of bytes to be sent.

Once this is sent, send the raw data directly after. Poll before sending
more.

## Protocol 7 {#protocol_7}

Protocol 7 is the method for communicating to Casio calcs over IO or
USB. The commands are sent in special command packets sent to the prizm
as raw data after sending a 0xC2 command as an MSD UFI command.
Confusing? Indeed. Below is the table showing you how the raw data sent
to the Prizm from the 0xC2 UFI commands is read:

|                  |            |                                            |                     |
|------------------|------------|--------------------------------------------|---------------------|
| Byte             | Field Name | Desciption                                 | Values              |
| 1                | Type       | The basic purpose of the packet            | 0x00 to 0x1F        |
| 2                | Subtype    | The specific function of the packet        | "00" to "57"        |
| 1                | Extended   | Decides if data size and data field follow | '0' - no; '1' - yes |
| (4, if extended) | Data size  | Size of data field                         | "0000" to "FFFF"    |
| (n, if extended) | Data       | Additional data related to packet function |                     |
| 2                | Checksum   | Checksum for integrity check               | "00" to "FF"        |

The protocol on the Prizm is not exposed in all modes. Currently, USB
Drive mode ignores Protocol 7 packets, but not the UFI packets. In
screen receiver, there is a role swap and the packet structure is
different. The only mode known to follow this is OS Update mode.

## Credits

BrandonW\
Simon Lother and Andreas Bertheussen for Protocol 7 info. PDF released
under Attribution-NonCommercial-NoDerivs 3.0 Unported (CC BY-NC-ND 3.0)
license, located [here](http://tny.im/dl/fxReverse2x.pdf).
