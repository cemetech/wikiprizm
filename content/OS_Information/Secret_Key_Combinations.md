---
revisions:
- author: Gbl08ma
  timestamp: '2017-07-04T12:02:44Z'
title: Secret Key Combinations
aliases:
  - /Secret_Key_Combinations/
---

The following is a list of key combinations that do unexpected things
like opening screens which are normally not accessible to the user.

All combinations work on the Prizm, and some may also work on other
Casio calculators.

## OS combinations {#os_combinations}

Combinations handled by the OS.

### Diagnostic mode {#diagnostic_mode}

-   Press **\[OPTN\]+\[EXP\]+\[AC/ON\]**
-   Press **\[F1\]** (no data will be deleted under normal conditions)
-   Press **\[9\]**

### Test mode {#test_mode}

There's also [a syscall for it]({{< ref "Syscalls/TestMode.md" >}}).

-   Press **\[OPTN\]+\[EXP\]+\[AC/ON\]**
-   Quickly type 5963

### Initialize all {#initialize_all}

-   Press **\[1\]+\[3\]+\[AC/ON\]**

A message asking "Reset OK? Initialize all." will appear.

### Delete current user name {#delete_current_user_name}

Available only on Prizm OS 1.04 and up. Possibly Casio got tired of
support requests on how to remove user names set, with unknown
"password", by people fooling the calculator owner.

-   Press **\[F2\]+\[Up\]+\[AC/ON\]**
-   Press **\[9\]**

*User name will be deleted. OK?* will appear.

**Note:** this doesn't actually delete the full user name, organization
and password log, it only adds an empty entry to the end of the log. The
previous user names, organizations and passwords are still saved in
flash and can be seen with an add-in like Utilities or INSIGHT. To
delete the full log, use the [appropriate bootloader
menu](#Flash_area_erase).

### Delete user name - alternative version {#delete_user_name___alternative_version}

The above key combination appears to not work on certain OS versions (OS
02.00 and up?). If it doesn't work, try this instead:

-   Turn off the calculator (**\[SHIFT\]** then **\[AC/ON\]**)
-   Press **\[OPTN\]+\[Up\]+\[AC/ON\]**
-   The screen should turn on. Release all keys.
-   Press **\[F1\]**

It is not known if, like the previous key combination, this merely
appends an empty entry to the end of the log, or if it completely clears
the user information sector. Utilities should help figure this out, if
you have the time.

### Enter examination mode {#enter_examination_mode}

Available only on Prizm OS 02.02 and up. See [Examination
mode]({{< ref "Examination_mode.md" >}}) for more information.

## Bootloader combinations {#bootloader_combinations}

Handled by the [bootloader](bootloader), these key combinations usually
involve pressing the RESTART button.

### Emergency OS updater {#emergency_os_updater}

Used to recover from a corrupted OS. Use any OS update bundle to send a
OS.

-   Press **\[F2\]+\[4\]+\[AC/ON\]+RESTART** at the same time
-   Release **RESTART** and continue pressing the remaining keys for a
    few seconds
-   Press **\[9\]** for a second
-   Press **\[×\] (multiplication)** for a second

"OS ERROR" will be shown on screen. The boot code's emergency OS updater
is now running.

### Flash area erase {#flash_area_erase}

Can delete specific flash areas and make a copy of the OS area into
another, probably overwriting part of the file system and corrupting it.

-   Press the **RESTART** button plus **\[OPTN\]+\[EXP\]+\[AC/ON\]** and
    hold all down
-   Release the **RESTART** button
-   Wait until the screen becomes white
-   Now release the remaining three keys

*Note: not pressing the restart button opens the [diagnostic
mode](#Diagnostic_mode).*

Now press and hold **\[MENU\]**. A menu will appear:

-   \[F1\] will Erase User Flash Area, clearing flash memory from
    0xA0C00000 to 0xA2000000.
-   \[F2\] will Erase [Password
    Area]({{< ref "User_Name_Registry.md" >}}), removing the user name
    and organization log, stored from 0xA0BE0000 to 0xA0C00000.
-   \[F3\] will Erase MCS Backup, clearing the user RAM backups on the
    flash from 0xA0B80000 to 0xA0BE0000.
-   \[F4\] will Erase Add-in Language, clearing the area reserved for
    the selected language add-in (from 0xA0B60000 to 0xA0B80000).
-   \[EXIT\] will make a copy of the OS to the area from 0xA1000000 to
    0xA1B60000 (may corrupt the file system!)

------------------------------------------------------------------------

[Source](http://www.omnimaga.org/casio-prizm/secret-debug-menu/)
