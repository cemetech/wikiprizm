---
bookCollapseSection: true
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:34:01Z'
title: Keyboard
---

This is a list of [syscalls]({{< ref "syscalls.md" >}}) related to the
[keyboard]({{< ref "keyboard.md" >}}) in some way. Most of them are for
reading keyboard input.
