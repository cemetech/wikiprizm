---
bookCollapseSection: true
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:35:06Z'
title: Serial
---

These are [syscalls]({{< ref "syscalls.md" >}}) used for manipulating
the 3-pin [serial]({{< ref "Technical_Documentation/serial.md" >}}) port
at a lower level, without a specific protocol in mind.

For higher level communication (*Protocol 7.00* and related), see the
[Communication category]({{< ref "Syscalls/Communication/" >}}).
