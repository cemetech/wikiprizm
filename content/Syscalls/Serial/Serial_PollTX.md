---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-01T10:51:12Z'
title: Serial_PollTX
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x1BC0\
**Function signature:** int Serial_PollTX(void)

Gets the free space in the serial transmit buffer.

## Returns

The number of free bytes in the transmit buffer, which has a size of 256
bytes.
