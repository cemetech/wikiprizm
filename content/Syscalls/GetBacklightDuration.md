---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-01T16:34:09Z'
title: GetBacklightDuration
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x12D9\
**Function signature:** char GetBacklightDuration()

Gets the amount of time after which the backlight dims if no key is
pressed.

## Returns

The amount of half minutes (30 second units) the calculator waits after
the last key press, before automatically reducing the brightness to the
lowest level one can set on the System menu.
