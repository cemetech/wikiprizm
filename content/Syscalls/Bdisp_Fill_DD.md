---
revisions:
- author: Gbl08ma
  timestamp: '2014-12-06T14:24:50Z'
title: Bdisp_Fill_DD
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0276\
**Function signature:** void Bdisp_Fill_DD(int color, int mode)

Fills different areas of the display with specified color, depending on
**mode**. This function is in effect a wrapper around
DirectDrawRectangle (see annotated disassembly below)

## Parameters

-   *int* **color** - color to fill the VRAM with
-   *int* **mode** - area of the VRAM to fill. If mode=1 draw a
    rectangle (6,24) to (389,215). If mode=2 the rectangle is drawn from
    (6,24) to (389,190). If mode=3 then the rectangle is drawn from
    (6,0) to (389,215) If mode=4 the rectangle appears to be drawn from
    (0,0) to (479,319). If any other value return from the function.

## Annotated disassembly {#annotated_disassembly}


    ! =============== S U B R O U T I N E =======================================


    Bdisp_Fill_DD:              ! CODE XREF: sub_8004C836+AA�p
    ! sub_8004C836+B2�p ...
            sts.l   pr, @-r15
            extu.b  r5, r0
            cmp/eq  #1, r0
            bt/s    loc_80053C44    ! Branch if mode=1
            mov r4, r1      ! Copy color into r1
            cmp/eq  #2, r0
            bt  loc_80053C4A    ! Branch if mode=2
            cmp/eq  #3, r0
            bt  loc_80053C56    ! Branch if mode=3
            cmp/eq  #4, r0
            bt  loc_80053C88    ! Branch if mode=4
            bra loc_80053C98    ! Default case do nothing
            nop
    ! ---------------------------------------------------------------------------

    loc_80053C44:               ! CODE XREF: Bdisp_Fill_DD+6�j
            mov #6, r4
            bra loc_80053C5A
            mov #24, r5
    ! ---------------------------------------------------------------------------

    loc_80053C4A:               ! CODE XREF: Bdisp_Fill_DD+C�j
            mov #0xFFFFFFBE, r7 ! y2
            mov #6, r4      ! x1
            extu.b  r7, r7      ! r7=0xBE 0xBE=190
            mov.w   #389, r6    ! x2
            bra loc_80053C90
            mov #24, r5     ! y1
    ! ---------------------------------------------------------------------------

    loc_80053C56:               ! CODE XREF: Bdisp_Fill_DD+10�j
            mov #6, r4      ! x1
            mov #0, r5      ! y1

    loc_80053C5A:               ! CODE XREF: Bdisp_Fill_DD+1C�j
            mov #0xFFFFFFD7, r7 ! y2
            mov.w   #389, r6    ! x2
            bra loc_80053C90
            extu.b  r7, r7      ! r7=0xD7 0xD7=215
    ! ---------------------------------------------------------------------------
    !Table of constants removed
    ! ---------------------------------------------------------------------------

    loc_80053C88:               ! CODE XREF: Bdisp_Fill_DD+14�j
            mov #0, r4      ! x1=0
            mov.w   #479, r6    ! x2
            mov.w   #319, r7    ! y2
            mov r4, r5      ! x1=y1=0

    loc_80053C90:               ! CODE XREF: Bdisp_Fill_DD+28�j
    ! Bdisp_Fill_DD+34�j
            extu.w  r1, r2      ! Zero extend color
            bsr DirectDrawRectangle
            mov.l   r2, @-r15   ! Pass color on the stack (note the delay slot this is executed before the branch)
            add #4, r15

    loc_80053C98:               ! CODE XREF: Bdisp_Fill_DD+16�j
            lds.l   @r15+, pr
            rts
            nop
    ! End of function Bdisp_Fill_DD
