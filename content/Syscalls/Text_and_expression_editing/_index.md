---
bookCollapseSection: true
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:35:49Z'
title: Text_and_expression_editing
---

This is a list of [syscalls]({{< ref "syscalls.md" >}}) that are related
to text and expression editing, and are involved in one or more of the
following tasks:

-   Drawing a string or math expression on VRAM, possibly with a cursor,
    for editing purposes ([PrintXY]({{< ref "Syscalls/PrintXY.md" >}}),
    for example, doesn't belong here);
-   Editing a string or math expression, adding or removing characters,
    changing the cursor position and other display parameters, in
    response to a key input (which is not necessarily captured by the
    syscall itself);
-   Letting the user select from a list of special characters;
-   Letting the user manipulate the clipboard contents.

[GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}}) plays an important
role in the usage of many of these syscalls, as the source for keyboard
input, but is not eligible for this category as its main focus is not
text or expression editing.
