---
revisions:
- author: Dr-carlos
  comment: Re-arrange paramater list
  timestamp: '2022-03-26T03:06:34Z'
title: PrintXY
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x18F9\
**Function signature:** void PrintXY(int x, int y, char\* message, int
mode, int color)

Draws a line of homescreen-style text in VRAM, with the specified
position and color.

## Parameters

-   **x** - The "homescreen" column of the first character to be drawn,
    1 to 20.
-   **y** - The "homescreen" row, 0 to 7.[^row0]
-   **message** - pointer to a buffer (possibly `const` and/or `static`)
    containing the message to display. The first two characters of the
    message are not displayed but are used to determine some properties
    of the string; see [message properties](#message-properties).
-   **mode** - controls whether to clear text background, or invert
    background and foreground colors, using a bitmask. See [modes](#modes) for details.
-   **color** - text color of the characters; see [colors](#colors).

[^row0]: Although row 0 is valid, it's usually under the status bar
at the top of the screen and won't be visible. If the status bar is
disabled, text in row 0 will be visible.

## Modes

Display mode 0 is normal, overwrite mode. The rectangle
containing each character is cleared. Setting bit 5 (0x20) makes the
background not be erased. Setting bit 0 (0x01) makes the text be written
in inverse mode. If you want both, bitwise OR the modes together.

You may also use the following, defined in *color.h*:

-   TEXT_MODE_NORMAL (0)
-   TEXT_MODE_INVERT (1)

When using 0x21 (0x20 \| 0x01), the effect isn't the expected where the
inverted text is drawn on top of the screen. Instead, it acts like an
AND operation where the printed text is used to mask what is currently
on the screen (color doesn't matter), printing white elsewhere.

## Colors

Valid colors are defined in *color.h* and are listed below:

-   TEXT_COLOR_BLACK (0) - Black
-   TEXT_COLOR_BLUE (1) - Blue
-   TEXT_COLOR_GREEN (2) - Green
-   TEXT_COLOR_CYAN (3) - Cyan
-   TEXT_COLOR_RED (4) - Red
-   TEXT_COLOR_PURPLE (5) - Purple
-   TEXT_COLOR_YELLOW (6) - Yellow
-   TEXT_COLOR_WHITE (7) - White

## Message properties

The first two bytes of the string are fed into
[ProcessPrintChars]({{% ref "ProcessPrintChars.md" %}}), which
allows the user to enable or disable GB 18030 text encoding, or simply do nothing. See
[CJK Text]({{% ref "Multi-byte_strings.md#cjk-text" %}}) for more detailed
information.

For displaying Latin characters, padding with two spaces is fine.
You should *never* attempt to save space by subtracting two bytes from the address of
your actual string because then you can't predict what bytes the function will
actually see and it could do something unexpected or crash.

## Examples

Displays "Hello, world!":

```
PrintXY("  Hello, world!");
```

Displays "你好!":

```
PrintXY("\x03\xa8\xc4\xe3\xba\xc3!");
```
