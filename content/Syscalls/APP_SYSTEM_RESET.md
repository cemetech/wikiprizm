---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T10:11:44Z'
title: APP_SYSTEM_RESET
---

## Synopsis

**Header:** fxcg/app.h\
**Syscall index:** 0x1E17\
**Function signature:** void APP_SYSTEM_RESET(void);

Opens the "RESET" screen that shows in the System menu (when F5 is
pressed).

## Comments

The operations take effect, be careful.

The syscall returns when the user presses EXIT.
