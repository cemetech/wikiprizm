---
bookCollapseSection: true
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:34:19Z'
title: Locale
---

[Locale]({{< ref "/OS_Information/Locale.md" >}})-related
[syscalls]({{< ref "syscalls.md" >}}).
