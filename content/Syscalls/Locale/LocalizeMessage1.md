---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-15T22:31:44Z'
title: LocalizeMessage1
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x12FC\
**Function signature:** int LocalizeMessage1(int msgno, char\* result)

Gets the message corresponding to the given number, in the language
selected by the user in the Language screen (System menu or start-up
wizard).

## Parameters

-   **msgno** - number of the message to display. To see all the
    messages and their numbers, use the
    [TestMode]({{< ref "Syscalls/TestMode.md" >}}) or an add-in like
    [this
    one](http://www.cemetech.net/scripts/countdown.php?/prizm/tools/PrintXY_2_Ref.zip&path=archives).
-   **result** - pointer to a buffer, with a size of at least 88 bytes,
    to which the [multi-byte
    string]({{< ref "Multi-byte_strings.md" >}}) will be copied.
