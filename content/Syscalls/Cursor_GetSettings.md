---
revisions:
- author: ProgrammerNerd
  timestamp: '2015-02-25T01:18:35Z'
title: Cursor_GetSettings
---

## Synopsis

**Header:** fxcg/display.h *(Not yet in
[libfxcg](https://github.com/Jonimoose/libfxcg))*\
**Syscall index:** 0x01F5\
**Function signature:** unsigned int Cursor_GetSettings(struct
cursorSettings\*);

Returns cursor settings.

## Parameters

Either an array of 17 bytes or a pointer to a variable of type
`struct cursorSettings`.


    struct __attribute__ ((__packed__)) cursorSettings{
        unsigned cursorX;
        unsigned cursorY;
        unsigned unk;
        unsigned cursorFlashFlag;
        unsigned char unkByte;
    };

## Returns

The same value found in cursorFlashFlag.

## Comments

cursorX and cursorY are set by
[locate_OS]({{< ref "Syscalls/locate_OS.md" >}}) or
[Cursor_SetPosition]({{< ref "Syscalls/Cursor_SetPosition.md" >}}).

cursorFlashFlag are set by
[Cursor_SetFlashMode_incompatible](Cursor_SetFlashMode_incompatible),
[Cursor_SetFlashOn]({{< ref "Syscalls/Cursor_SetFlashOn.md" >}}) or
[Cursor_SetFlashOff]({{< ref "Syscalls/Cursor_SetFlashOff.md" >}}) and
most likely by other functions.
