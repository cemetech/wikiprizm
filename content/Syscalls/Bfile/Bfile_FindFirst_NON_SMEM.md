---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-30T10:40:13Z'
title: Bfile_FindFirst_NON_SMEM
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1DB6\
**Function signature:** int Bfile_FindFirst_NON_SMEM(const unsigned
short\* pathname, int\* FindHandle, const unsigned short\* foundfile,
file_type_t\* fileinfo)

Initializes a search on a device other than the storage memory for a
list of files and folders and retrieves info about each. Probably has no
practical application on the Prizm, since there is no SD card slot and
the main memory doesn't count as a device (use the
[MCS]({{< ref "Syscalls/MCS/" >}}) syscalls instead).

## Parameters

-   *const unsigned short\** **pathname** - The 16 bit string containing
    the path of which to search (you may use one '\*' as a wildcard, ex:
    "\\\\crd0\\\*.g3a"). You must prefix the device identifier to the
    path, such as \\\\crd0\\.\
-   *int\** **FindHandle** - Search handle. This *must* be closed using
    [Bfile_FindClose]({{< ref "Syscalls/Bfile/Bfile_FindClose.md" >}})
    when you're done finding files.\
-   *const unsigned short\** **foundfile** - Will contain the file or
    folder name as a 16 bit string. Doesn't contain the full path.\
-   *file_type_t\** **fileinfo** - Pointer to a *file_info_t* struct.
    The members of this struct are not used as documented on
    [Bfile_FindFirst]({{< ref "Syscalls/Bfile/Bfile_FindFirst.md" >}})!

## Returns

0 on success, or a negative error code on failure.

## Comments

This syscall is probably useful on a system with SD card slot, for
example a future model of the Prizm. However, there should be no need to
use it directly, as syscall
[Bfile_FindFirst]({{< ref "Syscalls/Bfile/Bfile_FindFirst.md" >}}) will
automatically call this one when a device other than the storage memory
is used.

Using [Bfile]({{< ref "Syscalls/Bfile/" >}}) functions while user
[timers]({{< ref "/OS_Information/Timers.md" >}}) are installed can cause SYSTEM ERRORs
and other undefined behavior, especially with functions that change data
on the file system. Make sure to stop and uninstall all timers before
using Bfile functions (and optionally restore them later). See
[Incompatibility_between_Bfile_Syscalls_and_Timers]({{< ref "Incompatibility_between_Bfile_Syscalls_and_Timers.md" >}})
for more information.
