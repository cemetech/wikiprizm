---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = LongToAscHex \\| header\n\
    = fxcg/misc.h \\| index = 0x1841 \\| signature = void LongToAscHex(int\nvalue,\
    \ unsigned char\\* result, int length) \\| synopsis = Converts a\n\\<\u2026\u201D"
  timestamp: '2014-07-31T17:38:30Z'
title: LongToAscHex
---

## Synopsis

**Header:** fxcg/misc.h\
**Syscall index:** 0x1841\
**Function signature:** void LongToAscHex(int value, unsigned char\*
result, int length)

Converts a `long int` or `int` (both have the same size on the Prizm, 4
bytes) to its hexadecimal representation with a specified length.

## Parameters

-   **value** - value to convert.
-   **result** - pointer to string that will hold the conversion result.
-   **length** - desired length of the hexadecimal representation.
