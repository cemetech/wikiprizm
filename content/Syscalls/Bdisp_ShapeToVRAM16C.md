---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = Bdisp_ShapeToVRAM16C\n\\\
    | index = 0x01C4 \\| signature = void Bdisp_ShapeToVRAM16C(struct\ndisplay_shape\
    \ \\* shape, int color) \\| header = fxcg/display.h \\|\nsynopsis\u2026\u201D"
  timestamp: '2014-07-29T12:00:58Z'
title: Bdisp_ShapeToVRAM16C
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x01C4\
**Function signature:** void Bdisp_ShapeToVRAM16C(struct display_shape
\* shape, int color)

Draws a shape to VRAM.

## Parameters

-   **shape** - shape to draw to screen

    ```
    struct display_shape {
        int dx;
        int dy;
        int wx;
        int wy;
        int color;
        void* saved;
    };
    ```

-   **color** - indexed color (like
    [PrintXY]({{< ref "Syscalls/PrintXY.md" >}})) of the shape.
