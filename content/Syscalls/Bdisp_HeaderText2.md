---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T19:05:34Z'
title: Bdisp_HeaderText2
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1D85\
**Function signature:** void Bdisp_HeaderText2(void)

The function of this syscall is still unknown.

## Comments

Yet to know what it does --[Gbl08ma](User:Gbl08ma)
([talk](User_talk:Gbl08ma)) 10:44, 9 August 2012 (EDT)
