---
title: WordToHex
---

## Synopsis

**Header:** fxcg/misc.h\
**Syscall index:** 0x1348\
**Function signature:** `void WordToHex(unsigned short value, unsigned char* result)`

Converts a `unsigned short` to its hexadecimal representation.

## Parameters

-   **value** - `unsigned short` to convert.
-   **result** - pointer to the string that will hold the result.
