---
revisions:
- author: ProgrammerNerd
  timestamp: '2015-02-23T00:10:59Z'
title: Bdisp_EnableColor
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0921\
**Function signature:** int Bdisp_EnableColor(int n);

Switches the screen between full color mode (16 bits per pixel, RGB565)
and indexed color mode (3 bits per pixel, 8 colors - the same that can
be used with [PrintXY]({{< ref "Syscalls/PrintXY.md" >}})).

## Parameters

-   **n**: 0 to disable full color, 1 to enable full color.

## Returns

The result of [Bdisp_IsZeroDDRegisterB](Bdisp_IsZeroDDRegisterB) (see
disassembly for details).

## Comments

One can switch between the two color modes as needed, not just when an
add-in starts.

This function does not yield immediate results. If you want that use
[Bdisp_SetDDRegisterB]({{< ref "Syscalls/Bdisp_SetDDRegisterB.md" >}}).
You can call
[Bdisp_FrameAndColor]({{< ref "Syscalls/Bdisp_FrameAndColor.md" >}}) or
a function that uses
[Bdisp_FrameAndColor]({{< ref "Syscalls/Bdisp_FrameAndColor.md" >}})
such as [GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}}).

Most of the time, the OS works with the indexed color mode to reduce
memory requirements and increase speed. Exceptions include the main
menu, whenever a g3p picture is displayed, and screens with a
theme-related background like the eActivity file browser. Most custom
add-ins work always in full color mode regardless of needing any color
other than the eight indexed ones - this is a often forgotten
[optimization]({{< ref "Optimization_Tips.md" >}}) tip.

To get a better understanding of what really happens see the
disassembly.

    Bdisp_EnableColor:

    colorType   = -0xC

            mov.l   r14, @-r15
            sts.l   pr, @-r15
            add #-4, r15
            mov.l   #Bdisp_IsZeroDDRegisterB, r2
            jsr @r2 ! Bdisp_IsZeroDDRegisterB
            mov.l   r4, @(0xC+colorType,r15)
            mov.l   @(0xC+colorType,r15), r4
            extu.b  r0, r14
            bsr Bdisp_ColorAndFrameSetFlags
            mov #7, r5
            mov r14, r0
            add #4, r15
            lds.l   @r15+, pr
            rts
            mov.l   @r15+, r14
    ! End of function Bdisp_EnableColor
