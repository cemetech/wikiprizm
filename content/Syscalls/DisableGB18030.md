---
title: DisableGB18030
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x01DF\
**Function signature:** void DisableGB18030();

Sets following text display syscalls to use default text encoding.
See [multi-byte strings]({{% ref "Multi-byte_strings.md#cjk-text" %}})
for details.